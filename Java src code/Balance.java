package com.example.refkah9606.a301;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Refkah9606 on 2018-03-16.
 */
public class Balance implements Parcelable {

    public int balance;

    public Balance(){}

    private Balance (Parcel in) {
        balance = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flag){out.writeInt(balance);}

    @Override
    public String toString() {
        return String.valueOf(balance);
    }

    public static final Parcelable.Creator<Balance> CREATOR = new Parcelable.Creator<Balance>(){
        public Balance createFromParcel(Parcel in){ return new Balance(in);}
        public Balance [] newArray(int size){ return new Balance[size];}
    };

}