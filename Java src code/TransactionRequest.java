package com.example.refkah9606.a301;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Refkah9606 on 2018-03-16.
 */

public class TransactionRequest extends StringRequest {
    private static final String TRANSACTION_URL = "https://refka.x10host.com/transaction.php";
    private Map<String, String> params;

    public TransactionRequest(String key, String accountID, int amount, String countryCode, Response.Listener<String> listener) {
        super(Request.Method.POST, TRANSACTION_URL, listener, null);
        params = new HashMap<>();
        System.out.println(accountID + amount);
        // CHANGE IT TO THE TO ACCOUNT
        params.put("to", accountID);
        params.put("amount",String.valueOf(amount));
        params.put("countryCode", countryCode);
        params.put("key", key);

        CookieManager manager = new CookieManager();
        CookieHandler.setDefault(manager);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
