package com.example.refkah9606.a301;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import android.content.*;
import java.lang.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class AccountPage3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_page3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //buttons
        final Button accountButton1 = (Button) findViewById(R.id.accountButton1);
        final Button transactionButton = (Button) findViewById(R.id.transactionButton);
        final Button signOut = (Button) findViewById(R.id.signoutButton);

        // Add functionality to buttons
        accountButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(AccountPage3.this, AccountPage4.class);
                AccountPage3.this.startActivity(intent);
            }
        });

        accountButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String key = getIntent().getStringExtra("key");
                final RequestQueue queue = Volley.newRequestQueue(AccountPage3.this);

                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse (String response){
                        System.out.println(response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            ArrayList<Transaction> List = new ArrayList<Transaction>();
                            final String key = getIntent().getStringExtra("key");
                            if (success) {
                                JSONArray transactions = jsonResponse.getJSONArray("transactions");
                                for (int i = 0; i < transactions.length(); i++) {
                                    final JSONObject json_data = transactions.getJSONObject(i);
                                    //params.put("Transaction");
                                    DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
                                    Transaction transactionIguess = new Transaction();
                                    transactionIguess.Date = format.parse(json_data.getString("Date"));
                                    transactionIguess.TransactionID = json_data.getString("TransactionID");
                                    transactionIguess.Amount = json_data.getInt("Amount");

                                    List.add(transactionIguess);
                                    //Pass list to account page4

                                }

                                final Intent intent = new Intent(AccountPage3.this, AccountPage4.class);
                                intent.putParcelableArrayListExtra("Transaction", List);
                                intent.putExtra("key", key);
                                Response.Listener<String> responseListener2 = new Response.Listener<String>(){
                                    @Override
                                    public void onResponse (String response){
                                        try {
                                            JSONObject jsonResponse = new JSONObject(response);
                                            boolean success = jsonResponse.getBoolean("success");
                                            ArrayList<Balance> List = new ArrayList<Balance>();

                                            if (success) {
                                                JSONArray balances = jsonResponse.getJSONArray("balance");
                                                for (int i = 0; i < balances.length(); i++) {
                                                    final JSONObject json_data = balances.getJSONObject(i);
                                                    //params.put("Transaction");
                                                    Balance balance1 = new Balance();
                                                    balance1.balance = json_data.getInt("balance");

                                                    List.add(balance1);
                                                    //Pass list to account page4

                                                }
                                                intent.putParcelableArrayListExtra("Balance", List);
                                                intent.putExtra("key", key);
                                                AccountPage3.this.startActivity(intent);
                                            }

                                            else{
                                                AlertDialog.Builder builder = new AlertDialog.Builder(AccountPage3.this);
                                                builder.setNegativeButton("Retry", null);
                                                builder.create();
                                                builder.show();
                                            }
                                        } catch (JSONException e) {
                                            System.out.println(e.toString());
                                        }

                                    }
                                };

                                BalanceRequest accountPageRequest = new BalanceRequest(key, responseListener2);
                                queue.add(accountPageRequest);
                            }

                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(AccountPage3.this);
                                builder.setNegativeButton("Retry", null);
                                builder.create();
                                builder.show();
                            }
                        } catch (JSONException e) {
                            System.out.println(e.toString());
                        }
                        catch (ParseException e){

                        }

                    }
                };
                AccountPageRequest accountPageRequest = new AccountPageRequest(key, responseListener);
                queue.add(accountPageRequest);
            }
        });


        // Add functionality to buttons
        transactionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(AccountPage3.this, TransactionPage.class);
                AccountPage3.this.startActivity(intent);
            }
        });

        transactionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String key = getIntent().getStringExtra("key");
                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse (String response){
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            ArrayList<Accounts> accList = new ArrayList<Accounts>();

                            if (success == true) {
                                JSONArray accountID = jsonResponse.getJSONArray("accountID");
                                for (int i = 0; i < accountID.length(); i++) {
                                    final JSONObject json_data = accountID.getJSONObject(i);
                                    //params.put("accountID");

                                    Accounts accounts = new Accounts();
                                    accounts.accountID = json_data.getString("accountID");
                                    accList.add(accounts);
                                    //Pass list to transaction page
                                }

                                Intent intent = new Intent(AccountPage3.this, TransactionPage.class);
                                intent.putParcelableArrayListExtra("accountID", accList);
                                intent.putExtra("key", key);
                                AccountPage3.this.startActivity(intent);
                            }

                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(AccountPage3.this);
                                builder.setNegativeButton("Retry", null);
                                builder.create();
                                builder.show();
                            }
                        }
                        catch (JSONException e) {
                            System.out.println(e.toString());
                        }

                    }
                };

                AccountListRequest accountPageRequest = new AccountListRequest(key, responseListener);
                RequestQueue queue = Volley.newRequestQueue(AccountPage3.this);
                queue.add(accountPageRequest);


            }
        });

        // Add functionality to buttons
        signOut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                final String key = getIntent().getStringExtra("key");
                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse (String response){
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success == true) {
                                Intent intent = new Intent(AccountPage3.this, MainActivity.class);
                                intent.putExtra("key", key);
                                AccountPage3.this.startActivity(intent);
                            }

                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(AccountPage3.this);
                                builder.setNegativeButton("Retry", null);
                                builder.create();
                                builder.show();
                            }
                        }
                        catch (JSONException e) {
                            System.out.println(e.toString());
                        }

                    }
                };

                EndSessionRequest endSessionRequest = new EndSessionRequest(key, responseListener);
                RequestQueue queue = Volley.newRequestQueue(AccountPage3.this);
                queue.add(endSessionRequest);


            }
        });

        // Information

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(view, "More Information", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        Intent intent = new Intent(AccountPage3.this, Information.class);
                        AccountPage3.this.startActivity(intent);
                    }
                });
            }
        }
