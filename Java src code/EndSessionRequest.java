package com.example.refkah9606.a301;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import java.util.HashMap;
import java.util.Map;
import com.android.volley.Response;
import com.android.volley.Request;

public class EndSessionRequest extends StringRequest{
    private static final String EndSessionRequest_URL = "https://refka.x10host.com/session_destroy.php";
    private Map<String, String> params;

    public EndSessionRequest(String key, Response.Listener<String> listener){
        super(Method.POST, EndSessionRequest_URL, listener, null);
        params = new HashMap<>();
        params.put("key", key);

    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
