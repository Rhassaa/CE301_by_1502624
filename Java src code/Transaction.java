package com.example.refkah9606.a301;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.R.attr.format;

/**
 * Created by Refkah9606 on 2018-03-12.
 */

public class Transaction implements Parcelable {

    public String TransactionID;
    public int Amount;
    public java.util.Date Date;

    public Transaction(){}

    private Transaction(Parcel in) {
        TransactionID = in.readString();
        Amount = in.readInt();

        DateFormat format = new SimpleDateFormat("dd.MM.yy");
        try {
            Date = format.parse(in.readString());
        } catch (ParseException e) {
            Date = new Date(0, 0, 0);
        }

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flag){
        out.writeString(TransactionID);
        out.writeInt(Amount);
        DateFormat format = new SimpleDateFormat("dd.MM.yy");
        out.writeString(format.format(Date));
    }

    @Override
    public String toString() {
        DateFormat format = new SimpleDateFormat("dd.MM.yy");
        String f = format.format(Date);
        String d = String.valueOf(f);
        return String.format("%-50s %12s %-3d", TransactionID, d, Amount);

    }
    public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>(){
        public Transaction createFromParcel(Parcel in){ return new Transaction(in);}
        public Transaction [] newArray(int size){ return new Transaction[size];}

    };


}
