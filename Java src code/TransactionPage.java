package com.example.refkah9606.a301;

import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import android.content.*;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TransactionPage extends AppCompatActivity {

    public Location location = null;
    public LocationManager locationManager;
    public LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);

        // Radio buttons for send to accounts
        final RadioButton to = (RadioButton) findViewById(R.id.radioButton);
        final RadioButton to2 = (RadioButton) findViewById(R.id.radioButton2);
        final RadioButton to3 = (RadioButton) findViewById(R.id.radioButton3);
        final RadioButton to4 = (RadioButton) findViewById(R.id.radioButton4);
        final RadioButton to5 = (RadioButton) findViewById(R.id.radioButton5);
        final RadioButton to6 = (RadioButton) findViewById(R.id.radioButton6);

        //Make transaction button
        final Button makeTransAct = (Button) findViewById(R.id.transactButton);

        // amount field
        final EditText amount = (EditText) findViewById(R.id.tranAmount);

        // Get account lists
        List<Accounts> acc = getIntent().getParcelableArrayListExtra("accountID");

        String s1 = String.valueOf(acc.get(0));
        to.setText(s1);

        String s2 = String.valueOf(acc.get(1));
        to2.setText(s2);

        String s3 = String.valueOf(acc.get(2));
        to3.setText(s3);

        String s4 = String.valueOf(acc.get(3));
        to4.setText(s4);

        String s5 = String.valueOf(acc.get(4));
        to5.setText(s5);

        String s6 = String.valueOf(acc.get(5));
        to6.setText(s6);

        final RadioButton[] accounts = new RadioButton[]{to, to2, to3, to4, to5, to6};

        // LOCATION
        getSystemService(Context.LOCATION_SERVICE);

        locationListener = new TransactionLocationListener(this);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            AlertDialog.Builder builder = new AlertDialog.Builder(TransactionPage.this);
            builder.setMessage("Transaction failed, no location accessed");
            builder.setNegativeButton("Retry", null);
            builder.create();
            builder.show();
            Intent intent = new Intent(TransactionPage.this, AccountPage3.class);
            TransactionPage.this.startActivity(intent);
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);

        // Make transaction button
        makeTransAct.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final int am = Integer.parseInt(amount.getText().toString());
                if(location != null) {
                    Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                        final String key = getIntent().getStringExtra("key");
                        final Response.Listener<String> responseListener = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonResponse = new JSONObject(response);
                                    boolean success = jsonResponse.getBoolean("success");

                                    if (success) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(TransactionPage.this);
                                        builder.setMessage("Transaction was successful");
                                        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intent = new Intent(TransactionPage.this, AccountPage3.class);
                                                intent.putExtra("amount", am);
                                                intent.putExtra("key", key);
                                                TransactionPage.this.startActivity(intent);
                                            }});
                                        builder.create();
                                        builder.show();


                                    }

                                    else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(TransactionPage.this);
                                        builder.setMessage("Transaction failed");
                                        builder.setNegativeButton("Retry", null);
                                        builder.create();
                                        builder.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        };
                        String accountID = "";
                        for (int i = 0; i < accounts.length; i++) {
                            if (accounts[i].isChecked())
                                accountID = accounts[i].getText().toString();
                        }
                        TransactionRequest transactionRequest = new TransactionRequest(key, accountID, am, addresses.get(0).getCountryCode(), responseListener);
                        transactionRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue queue = Volley.newRequestQueue(TransactionPage.this);
                        queue.add(transactionRequest);
                        System.out.println(addresses.get(0).getCountryCode());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(TransactionPage.this);
                    builder.setMessage("Transaction failed, no location accessed");
                    builder.setNegativeButton("Retry", null);
                    builder.create();
                    builder.show();

                }

                ;

        // information button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "More Information", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(TransactionPage.this, Information.class);
                TransactionPage.this.startActivity(intent);
            }
        });
    }

});}}

