package com.example.refkah9606.a301;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.*;
import android.widget.EditText;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginPage2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // The field for the verification PIN sent via SMS
        final EditText verificationPIN = (EditText) findViewById(R.id.verificationPIN);
        // The button to continue to next page, verifying the PIN
        final Button logInverification = (Button) findViewById(R.id.logInverification);

        // Listener for log in button
        logInverification.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent accountpage3 = new Intent(LoginPage2.this, AccountPage3.class);
                LoginPage2.this.startActivity(accountpage3);
            }
        });

        logInverification.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String key = getIntent().getStringExtra("key");
                final String pin = verificationPIN.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse (String response){
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success){
                                Intent intent = new Intent(LoginPage2.this, AccountPage3.class);
                                intent.putExtra("PIN", pin);
                                intent.putExtra("key", key);
                                LoginPage2.this.startActivity(intent);
                            }
                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginPage2.this);
                                builder.setMessage("Login failed");
                                builder.setNegativeButton("Retry", null);
                                builder.create();
                                builder.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };

                PINRequest pinRequest = new PINRequest(key, pin, responseListener);
                RequestQueue queue = Volley.newRequestQueue(LoginPage2.this);
                queue.add(pinRequest);
            }
        });

        // information
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "More Information", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        Intent intent = new Intent(LoginPage2.this, Information.class);
                        LoginPage2.this.startActivity(intent);
            }
        });
    }

}
