package com.example.refkah9606.a301;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import java.util.HashMap;
import java.util.Map;
import com.android.volley.Response;
import com.android.volley.Request;

public class PINRequest extends StringRequest {
    private static final String PIN_URL = "https://refka.x10host.com/sms_inst.php";
    private Map<String, String> params;

    public PINRequest(String key, String pin, Response.Listener<String> listener) {
        super(Request.Method.POST, PIN_URL, listener, null);
        params = new HashMap<>();
        params.put("key", key);
        params.put("PIN", pin);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}