package com.example.refkah9606.a301;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by Refkah9606 on 2018-03-17.
 */

public class TransactionLocationListener implements LocationListener {
    private TransactionPage tp = new TransactionPage();

    public TransactionLocationListener(TransactionPage page){
        tp = page;
    }

    @Override
    public void onLocationChanged(Location location){
        tp.location = location;

    }
    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

}
