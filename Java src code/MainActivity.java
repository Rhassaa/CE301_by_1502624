package com.example.refkah9606.a301;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import android.content.*;
import java.lang.*;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // The login in field, the phone number
        final EditText clientID = (EditText) findViewById(R.id.logInID);
        // Password field
        final EditText password = (EditText) findViewById(R.id.password1);

        // button to confirm log in, takes to next page, LoginPage2
        final Button logInButton1 = (Button) findViewById(R.id.LogInButton1);
        // checked textview, takes to information page
        final CheckedTextView info = (CheckedTextView) findViewById(R.id.info);

        // Listener for log in button

        // Add functionality to buttons
        // Add functionality to buttons
        logInButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, LoginPage2.class);
                MainActivity.this.startActivity(intent);
            }
        });

        logInButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String client = clientID.getText().toString();
                final String pass = password.getText().toString();
                System.out.println("NOOOOOO");

                Response.Listener<String> responseListener = new Response.Listener<String>(){

                    public void onResponse (String response){
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success == true){
                                Intent intent = new Intent(MainActivity.this, LoginPage2.class);
                                intent.putExtra("clientID", client);
                                String key = jsonResponse.getString("key");
                                intent.putExtra("key", key);
                                MainActivity.this.startActivity(intent);
                                System.out.println("ble");

                            }
                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage("Login failed");
                                builder.setNegativeButton("Retry", null);
                                builder.create();
                                builder.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };



                LoginRequest loginRequest = new LoginRequest(client, pass, responseListener);
                loginRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                queue.add(loginRequest);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Information", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        Intent intent = new Intent(MainActivity.this, Information.class);
                        MainActivity.this.startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
