package com.example.refkah9606.a301;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Refkah9606 on 2018-03-16.
 */

public class BalanceRequest extends StringRequest {

    private static final String BALANCE_REQUEST = "https://refka.x10host.com/balance.php";
    private Map<String, String> params;

    public BalanceRequest(String key, Response.Listener<String> listener) {
        super(Request.Method.POST, BALANCE_REQUEST , listener, null);
        params = new HashMap<>();
        params.put("key", key);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }



}
