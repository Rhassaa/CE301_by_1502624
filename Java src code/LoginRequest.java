package com.example.refkah9606.a301;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import java.util.HashMap;
import java.util.Map;
import com.android.volley.Response;
import com.android.volley.Request;
import java.net.*;

public class LoginRequest extends StringRequest {
    private static final String LOGIN_REQUEST_URL = "https://refka.x10host.com/login_inst.php";
    private Map<String, String> params;

    public LoginRequest(String clientID, String PASSWORD, Response.Listener<String> listener) {
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("clientID", clientID);
        params.put("PASSWORD", PASSWORD);
        System.out.println("yee");

        CookieManager manager = new CookieManager();
        CookieHandler.setDefault(manager);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
