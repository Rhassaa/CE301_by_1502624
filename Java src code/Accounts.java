package com.example.refkah9606.a301;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Refkah9606 on 2018-03-13.
 */

public class Accounts implements Parcelable {

    public String accountID;

    public Accounts(){}

    private Accounts(Parcel in) {
        accountID = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flag){
        out.writeString(accountID);
    }

    @Override
    public String toString() {
        return String.valueOf(accountID);
    }

    public static final Parcelable.Creator<Accounts> CREATOR = new Parcelable.Creator<Accounts>(){
        public Accounts createFromParcel(Parcel in){ return new Accounts(in);}
        public Accounts [] newArray(int size){ return new Accounts[size];}
    };

}

