package com.example.refkah9606.a301;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AlertDialog;
import android.widget.*;
import android.content.*;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class AccountPage4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_page4);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Transaction history fields
        final TextView transAction7 = (TextView) findViewById(R.id.textView7);
        final TextView transAction8 = (TextView) findViewById(R.id.textView8);
        final TextView transAction9 = (TextView) findViewById(R.id.textView9);
        final TextView transAction10 = (TextView) findViewById(R.id.textView10);
        final TextView transAction11 = (TextView) findViewById(R.id.textView11);
        final TextView transAction12 = (TextView) findViewById(R.id.textView12);
        final TextView transAction13 = (TextView) findViewById(R.id.textView13);
        final TextView transAction14 = (TextView) findViewById(R.id.textView14);
        final TextView transAction15 = (TextView) findViewById(R.id.textView15);

        //balance field
        final TextView bal = (TextView) findViewById(R.id.textView4);

        // take in values for list and balance
        List<Transaction> list = getIntent().getParcelableArrayListExtra("Transaction");

        List<Balance> balance = getIntent().getParcelableArrayListExtra("Balance");

        Balance balen = balance.get(0);
        bal.setText(((Integer) balen.balance).toString());

        String s1 = String.valueOf(list.get(0));
        transAction7.setText(s1);

        String s2 = String.valueOf(list.get(1));
        transAction8.setText(s2);

        String s3 = String.valueOf(list.get(2));
        transAction9.setText(s3);

        String s4 = String.valueOf(list.get(3));
        transAction10.setText(s4);

        String s5 = String.valueOf(list.get(4));
        transAction11.setText(s5);

        String s6 = String.valueOf(list.get(5));
        transAction12.setText(s6);

        String s7 = String.valueOf(list.get(6));
        transAction13.setText(s7);

        String s8 = String.valueOf(list.get(7));
        transAction14.setText(s8);

        String s9 = String.valueOf(list.get(8));
        transAction15.setText(s9);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "More Information", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(AccountPage4.this, Information.class);
                AccountPage4.this.startActivity(intent);
            }
        });
    }

}
