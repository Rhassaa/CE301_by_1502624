package com.example.refkah9606.a301;

import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.toolbox.*;

public class AccountPageRequest extends StringRequest {

    private static final String ACCOUNT_PAGE_REQUEST = "https://refka.x10host.com/accountHist_inst.php";
    private Map<String, String> params;

    public AccountPageRequest(String key, Response.Listener<String> listener) {
        super(Request.Method.POST, ACCOUNT_PAGE_REQUEST , listener, null);
        params = new HashMap<>();
        params.put("key", key);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}


