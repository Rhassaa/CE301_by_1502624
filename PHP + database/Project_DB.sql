/* sql database for the project */

CREATE DATABASE bankDB;

CREATE TABLE client(	
	clientID integer PRIMARY KEY,
	name text,
	phoneNo integer,
	password varchar (255),
	address varchar (255),
	postcode varchar (255),
	city text,
	country text,
    pin varchar (6)
);

CREATE TABLE account(
	accountID integer PRIMARY KEY,
	balance integer, 
	clientID integer, 
    FOREIGN KEY (clientID) REFERENCES client (clientID)
);

CREATE TABLE transaction(
	transactionID text PRIMARY KEY,
	amount integer,
	dat date,
	accountID integer, 
	FOREIGN KEY (accountID) REFERENCES account (accoundID)
);

/* Inserting values*/

/* Person 1 */
INSERT INTO client(clientID, name, phoneNo, password, address, postcode, city, country)
VALUES ('0001', 'Ann Anderson', '000 0000001', '$2y$14$gcyQ/zqCeVpa5zD8.rtu..WViilqSOdmCjG/02XQLhrpD7xHTpcQK', 'Not Real Street 1', '101', 'Faketown', 'Fakeny');

/* Person 2*/
INSERT INTO client(clientID, name, phoneNo, password, address, postcode, city, country)
VALUES ('0002', 'Benny Bent', '000 0000002', '$2y$14$u0cI9FGoIntsZejGRIbIueglU2c.e0/XbfHkdEm.Ddau8RP7Nn9le', 'Not Real Street 2', '202', 'Faketown', 'Fakeny');

/*Person 3*/
INSERT INTO client(clientID, name, phoneNo, password, address, postcode, city, country)
VALUES ('0003', 'Courtney Cal', '000 0000003', '$2y$14$5vI41Zd9Xu3lvBOBSl6uqOICY6qAwiIgphn8aAr79wrYpg2ePLH2e', 'Not Real Street 3', '303', 'Faketown', 'Fakeny');

/*Person 4*/
INSERT INTO client(clientID, name, phoneNo, password, address, postcode, city, country)
VALUES ('0004', 'Dani Don', '000 0000004', '$2y$14$ejCi4VpSIwRQMOO5Fjmg0.wIISS8sCMNRhArhIhYtzmAvBd3Zjmxu', 'Not Real Street 4', '404', 'Faketown', 'Fakeny');

/*Person 5*/
INSERT INTO client(clientID, name, phoneNo, password, address, postcode, city, country)
VALUES ('0005', 'Emily Emmerson', '000 0000005', '$2y$14$o7P5ab5kK.K5upBEmTSUdea6sl5zXwwtVmQUy7Fa/KWhvj7ryJvNm', 'Not Real Street 5', '505', 'Faketown', 'Fakeny');

/*Person 6, me test dummy yo*/ 
INSERT INTO client(clientID, name, phoneNo, password, address, postcode, city, country)
VALUES ('0006', 'Refka Hassan', '07599468056', '$2y$14$rbsUgg0Okrx/PTkh0GLqAOUI9pcINh3qkER5X8DPDcIfSNqqW0A.C', 'Not Real Street 6', '606', 'Faketown', 'Fakeny');

/* Perons 1's accounts */ 
INSERT INTO account(accountID, balance, clientID)
VALUES ('01', '40,000.00', '0001');

INSERT INTO account(accountID, balance, clientID)
VALUES ('02', '120,000.00', '0001');

/* Peron 2's accounts */ 
INSERT INTO account(accountID, balance, clientID)
VALUES ('03', '10,000.00', '0002');

INSERT INTO account(accountID, balance, clientID)
VALUES ('04', '100.00', '0002');

INSERT INTO account(accountID, balance, clientID)
VALUES ('05', '10.00', '0002');

/* Person 3's accounts*/
INSERT INTO account(accountID, balance, clientID)
VALUES ('06', '7,000.00', '0003');

INSERT INTO account(accountID, balance, clientID)
VALUES ('07', '10.00', '0003');

/*Person 4's accounts*/
INSERT INTO account(accountID, balance, clientID)
VALUES ('08', '9000.00', '0004');

/*Person 5's account*/
INSERT INTO account(accountID, balance, clientID)
VALUES ('09', '700.00', '0005');

/*Person 6's accounts*/
INSERT INTO account(accountID, balance, clientID)
VALUES ('10', '70.00', '0006');

INSERT INTO account(accountID, balance, clientID)
VALUES ('11', '12.00', '0006');










