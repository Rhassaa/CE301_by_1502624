<?php

    function __autoload($class){
        require_once(str_replace('\\','/', $class) . '.php');
    }
    
    use FastSMS\Client;
    use FastSMS\Model\Message;
    use FastSMS\Exceptions\ApiException;
    
    $tokens = '1VIu-iDFx-3oOD-6b2F';

    $con = mysqli_connect('162.253.224.14', 'refkax10_rhassaa', '392467475', 'refkax10_db');

    $clientID = $_POST['clientID'];
    $PASSWORD = $_POST['PASSWORD'];

    $statement = mysqli_prepare($con, "SELECT clientID, PASSWORD, phoneNo FROM CLIENT WHERE clientID = ?");

    mysqli_stmt_bind_param($statement, 's', $clientID);
    mysqli_stmt_execute($statement);

    mysqli_stmt_store_result($statement);
    mysqli_stmt_bind_result($statement, $clientID, $hash, $phoneNo);
    
    $response = array();
    $response['success'] = false;
        
    if(mysqli_stmt_fetch($statement)){
        $response['clientID'] = $clientID;
        $response['PASSWORD'] = $PASSWORD;
        $response['success'] = password_verify($PASSWORD, $hash);
    }

    if($response['success']) {
        
        //generate pass
        $pin = mt_rand(0, 9) . mt_rand(0, 9) . mt_rand(0, 9) 
            . mt_rand(0, 9) . mt_rand(0, 9);
        
        //send to database
        $statement = mysqli_prepare($con, "UPDATE CLIENT SET pin = ? WHERE clientID = ?");
        if(!$statement){
            $response['success'] = false;
        }
        else {
            mysqli_stmt_bind_param($statement, 'ss', $pin, $clientID);
            mysqli_stmt_execute($statement);
            //send pin via sms
            
            $client = new client($tokens);
            
            $data = [
                'destinationAddress' => $phoneNo,
                'sourceAddress' => 'Bank',
                'body' => 'Your PIN is: ' . $pin,
                'validityPeriod' => 3600 * 6
            ];
            
            try {
                $result = $client->message->send($data);
            }
            catch(Exception $exp){
                echo $exp->getMessage();
       
}           session_start();
            $_SESSION["clientID"] = $clientID;
            $_SESSION['authenticated'] = true;
            $_SESSION["verified"] = false;
            $response['key'] = session_id();
        
        }}

echo json_encode($response);


?>

